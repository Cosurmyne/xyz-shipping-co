namespace xyz
{
    public class IteratorDEF : Iterator
    {
        private DEF _aggregate;
        private int _position;

        public IteratorDEF(DEF aggregate)
        {
            _aggregate = aggregate;
            _position = 0;
        }

        public Product Current()
        {
		if (_position < _aggregate.Count()) return _aggregate[_position];
		return null;
        }

        public Product First()
        {
            _position = 0;
	    return Current();
        }

        public bool IsDone()
        {
		return _position >= _aggregate.Count();
        }

        public Product Next()
        {
		_position++;
		return Current();
        }
    }
}
