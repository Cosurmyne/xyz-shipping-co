namespace xyz
{
    public interface Base
    {
	    void GetList();
	    void TimeStamps();
	    void Create();
	    void Query();
    }
}
