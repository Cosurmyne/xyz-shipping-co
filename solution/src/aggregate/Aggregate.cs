using System;
using System.Collections.Generic;

namespace xyz
{
    public abstract class Aggregate
    {
        protected List<Product> _items = new List<Product>();

        public abstract Iterator CreateIterator();
        public abstract void Add(Product o);

        public double Weigh()
        {
            double weight = 0;
            foreach (var item in _items)
            {
                weight += item.Weight;
            }
            return weight;
        }
        public byte Count()
        {
            return (byte)_items.Count;
        }

        public void Display()
        {
            foreach (var item in _items)
            {
                Console.WriteLine("{0}", item.ToString());
            }
            Console.ReadKey();
        }
    }
}
