namespace xyz
{
    public class DEF : Aggregate
    {
        public Product this[int index]
        {
            get { return _items[index]; }
        }

        public override void Add(Product o)
        {
		_items.Add(o);
        }

        public override Iterator CreateIterator()
        {
            return new IteratorDEF(this);
        }

        public override string ToString()
        {
            return "DEF Foods";
        }

    }
}
