using System;

namespace xyz
{
    public class Shipment : Shipping
    {
        private string _description;
        private DateTime _time;
        private Aggregate _records;

        public Shipment(Aggregate records)
        {
            _time = DateTime.Now;
            _records = records;
            _description = records.ToString().PadRight(13) + _time;
        }

        public override string Description => _description;

        public override DateTime Time => _time;

        public override Aggregate Records => _records;

        public override void DisplayInfo(bool timestamps, bool root = false)
        {
            string title = String.Format("{0}", Description);
            Console.WriteLine();
            title = String.Format("{0} :: {1}\n{2}\n", title, 8, "".PadRight(title.Length + 6, '.'));
            title = String.Format("{0} Weight {1}kg : Qty {2}\n", title, _records.Weigh(), _records.Count());
            Console.WriteLine("{0} {1}\n{2}", title, Utensil.STAGE[8 - 1], "".PadRight(Utensil.STAGE[8 - 1].Length + 1, '-'));
        }
    }
}
