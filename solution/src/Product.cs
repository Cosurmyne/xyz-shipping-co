using System;

namespace xyz
{
    public class Product
    {
        private string desc;
        private double weight;

        public Product(string desc, double weight)
        {
            this.desc = desc;
            this.weight = weight;
        }

	public string Description { get{ return desc; } }
	public double Weight { get{ return weight; } }

        public override string ToString()
        {
		return String.Format("{0, -25}: {1}kg", Description, Weight);
        }
    }
}
